<?php

$array = [
    'key1'=>'hello',
    'key2'=>'world',
    'key3'=>'ajhdkj',
    'key4'=>'xys',
    'key5'=>'sjkdhskdjkw'
];

// $i = 0;
// foreach ($array as $key => $value) {
//     $i++;
// }
// echo $i;

function my_array_values($array) {
    $new_array = [];
    foreach($array as $value) {
        $new_array[] = $value;
    }
    return $new_array;
}

// var_dump(my_array_values($array));

function my_array_keys($array) {
    $new_array = [];
    foreach ($array as $key => $value) {
        $new_array[] = $key;
    }
    return $new_array;
}

// var_dump(my_array_keys($array));

function my_array_pop($array) {
    foreach($array as $value) {
        $result = $value;        
    }
    return $result;
}

// var_dump(my_array_pop($array));

// function my_array_push($array,$new_value) {
//     if(is_array($new_value)) {
//         foreach($new_value as $key => $value) {
//             $array[] = $value;
//         }
//     } else {
//         $array[] = $new_value;
//     }
//     return count($array);
// }

function my_array_push($array, $pushData) {
    if(is_array($pushData)) {
        return count($array) + count($pushData);
    } else {
        return count($array)+1;
    }
}

// var_dump(my_array_push($array,['sjds','dswioio','phan tu 3']));

function my_array_shift(&$array) {
    foreach($array as $key=>$value) {
        unset($array[$key]);
        return $value;
    }
}

// var_dump(my_array_shift($array));


function my_array_unshift(&$array, $pushData) {
    if(is_array($pushData)) {
        $new_array = [];
        $new_array = $pushData;
        foreach ($array as $key => $value) {
            $new_array[$key] = $value;
        }
    } else {
        $new_array = $pushData;
        $new_array[] = $array;
        
    }
    $array = $new_array;
    return count($new_array);
}

// var_dump(my_array_unshift($array, ['phan tu 1', 'phan tu 2']));


function my_array_flip($array) {
    $new_array = [];
    foreach($array as $key => $value) {
        $new_array[$value] = $key;
    }
    return $new_array;
}
// var_dump(my_array_flip($array));


// function plus($i) {
//     if($i <= 5) {
//         return plus($i+1);
//     } else {
//         return $i;
//     }
// }



// echo plus(1);


function my_sort($array, $new_array = []) {
    if(empty($array)) {
        $array = $new_array;
        return TRUE;
    } else {
        foreach($array as $key =>$value) {
            if (!is_numeric($value)) {
                return FALSE;
            }
            if(isset($min) && $min > $value) {
                $min = $value;
                $unset_key = $key;
            } elseif (empty($min)) {
                $min = $value;
                $unset_key = $key;
            }
        }
        $new_array[$unset_key] = $array[$unset_key];
        unset($array[$unset_key]);
        return my_sort($array, $new_array);
    }
}

$array = [
    1,
    5,
    4,
    3,
    8
];

my_sort($array);
var_dump($array);